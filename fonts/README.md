Scalable Screen Font 2.0 - Example Fonts
========================================

 - Free*.sfn - vector based font with cubic Bezier curves, converted from [GNU FreeFont](https://www.gnu.org/software/freefont/)'s OTF files (GPL with font embedding exception) ![](Free.png)

 - Vera*.sfn - vector based fonts with quadratic Bezier curves, converted from [GNOME Bitstream Vera Font](https://www.gnome.org/fonts/)'s TTF files (MIT license) ![](Vera.png)

 - Vera.sfn - an SSFN font collection of the above fonts

 - u_vga16.sfn.gz - bitmap font, converted from [u_vga16.bdf](http://www.inp.nsk.su/~bolkhov/files/fonts/univga/), made by Dmitry Bolkhovityanov (MIT license) ![](u_vga16.png)

 - unifont.sfn.gz bitmap font, converted from [unifont.hex](http://unifoundry.com/unifont/index.html) of the GNU unifont project (GPL with font embedding exception) ![](unifont.png)

 - creep.sfn.gz - bitmap font, converted from [creep.sfd](https://github.com/romeovs/creep/releases) (MIT license) ![](creep.png)

 - lanapixel.sfn.gz - bitmap font, 14px sized with pretty decent UNICODE coverage by eishiya from [LanaPixel_BitmapOnly.ttf](https://opengameart.org/content/lanapixel-localization-friendly-pixel-font) (CC-BY-4.0, despite what the original page states, this isn't 11px and isn't a pixel font) ![](lanapixel.png)

 - galmuri7.sfn.gz - bitmap font, 15px sized with an armada of CJK characters from [Galmuri7.kbitx](https://galmuri.quiple.dev/glyphs) (SIL license) ![](galmuri7.png)

 - chrome.sfn - pixmap font, Retro Chrome looking, made after an image found by Google (for demostration purposes only, do not use) ![](chrome.png)

 - emoji.sfn - pixmap font, created from a screenshot of [unicode.org's emoji page](http://www.unicode.org/emoji/charts/full-emoji-list.html) (for demostration purposes only, do not use) ![](emoji.png)

 - stoneage.sfn - pixmap font from an old DOS game, which I rewrote for Linux, [xstoneage](https://gitlab.com/bztsrc/xstoneage) (for demostration purposes only, do not use) ![](stoneage.png)

 - bende.sfn - my little boy's secret writing system, with ligatures, entirely made with sfnedit (MIT license) ![](bende.png)

 - Flexi.sfn - vector font from [Flexi_IBM_VGA_True.ttf](http://int10h.org/filez/Flexi_IBM_VGA_Fonts.zip) (PD, in TTF 257k, in SSFN 44k only) ![](Flexi.png)

 - VGA9.sfn.gz - bitmap font, converted from [ROM image](https://int10h.org/oldschool-pc-fonts/fontlist/font?ibm_vga_9x16) (for demostration purposes only, do not use) ![](VGA9.png)

 - bestfnt3.sfn - pixmap, diskmag font from [bestfnt3.bmf](https://bmf.php5.cz/index.php?font=bestfnt3&fullCharset=1) (PD) ![](bestfnt3.png)

 - amiga.sfn.gz - bitmap, original ROM font from [amiga-ks13-topaz-09.yaff](https://raw.githubusercontent.com/robhagemans/hoard-of-bitfonts/master/amiga/amiga-ks13-topaz-09.yaff) (PD) ![](amiga.png)

 - c64.sfn - pixmap, a C-64 demo font from [c64-64.bmf](https://bmf.php5.cz/index.php?font=c64-62&fullCharset=1) (PD) ![](c64.png)

 - xenon2.sfn - pixmap, a DOS game font from [xenon2.bmf](https://bmf.php5.cz/index.php?font=xenon2&fullCharset=1) (PD) ![](xenon2.png)
