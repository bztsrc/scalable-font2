/*
 * sfnedit/m_test.c
 *
 * Copyright (C) 2020 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Main window test font tool
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "libsfn.h"
#include "ui.h"
#include "lang.h"

#define FONTMAXSIZE 256

static char teststr[256] = { 0 };
int fontsize = 24;

/**
 * Font test window
 */
void view_test()
{
    ui_win_t *win = &wins[0];
    int r, x = 32, y = 80;
    char *str = teststr;

    ui_box(win, 19, 40, 20, 20, theme[wins[0].field == 7 ? THEME_CURSOR : (selfield == 0 ? THEME_DARKER : THEME_LIGHT)],
        theme[wins[0].field == 7 ? THEME_LIGHT : THEME_BG], theme[wins[0].field == 7 ? THEME_CURSOR :
        (selfield == 0 ? THEME_LIGHT : THEME_DARKER)]);
    ui_icon(win, 21, 42, ICON_ZOOMOUT, 0);
    ui_box(win, 41, 40, 20, 20, theme[wins[0].field == 8 ? THEME_CURSOR : (selfield == 1 ? THEME_DARKER : THEME_LIGHT)],
        theme[wins[0].field == 8 ? THEME_LIGHT : THEME_BG], theme[wins[0].field == 8 ? THEME_CURSOR :
        (selfield == 1 ? THEME_LIGHT : THEME_DARKER)]);
    ui_icon(win, 43, 42, ICON_ZOOMIN, 0);

    if(!teststr[0]) strcpy(teststr, "Lorem ipsum sic dolor amet");
    ui_input(win, 64, 40, win->w - 84, teststr, wins[0].field == 9, 255, 0);

    ssfn_dst.fg = theme[THEME_FG];
    ssfn_dst.w = win->w - 16;
    ssfn_dst.h = win->h;

    ui_box(win, 16, 76, win->w - 32, fontsize + 8, theme[THEME_BG], theme[THEME_BG], theme[THEME_BG]);
    do { r = ui_render(win, &x, &y, fontsize, str); str += r; } while(r > 0);
    ssfn_dst.w = win->w;
}

/**
 * On enter handler
 */
void ctrl_test_onenter()
{
    switch(wins[0].field) {
        case 7:
            if(fontsize > 8) fontsize--;
        break;
        case 8:
            if(fontsize < FONTMAXSIZE) fontsize++;
        break;
    }
}

/**
 * On key handler
 */
void ctrl_test_onkey()
{
    if(event.x == K_UP && fontsize < FONTMAXSIZE) fontsize++;
    if(event.x == K_DOWN && fontsize > 8) fontsize--;
    if(event.x >= ' ') {
        strcpy(teststr, (char*)&event.x);
        wins[0].field = 9;
        input_refresh = 1;
        input_maxlen = 0;
        input_str = NULL;
    }

    ui_refreshwin(0, 0, 0, wins[0].w, wins[0].h);
}

/**
 * On button press handler
 */
void ctrl_test_onbtnpress()
{
    ui_win_t *win = &wins[0];

    if(event.w & 24) {
        if(event.w & 8 && fontsize > 8) fontsize--;
        if(event.w & 16 && fontsize < FONTMAXSIZE) fontsize++;
    } else {
        selfield = wins[0].field = -1;
        if(event.y > 40 && event.y < 60) {
            if(event.x >= 19 && event.x < 39) selfield = 0; else
            if(event.x >= 41 && event.x < 61) selfield = 1; else
            if(event.x >= 64 && event.x < win->w - 20) wins[0].field = 9;
        }
    }
}

/**
 * On click (button release) handler
 */
void ctrl_test_onclick()
{
    if(event.y > 40 && event.y < 60) {
        if(event.x >= 19 && event.x < 39 && selfield == 0 && fontsize > 8) fontsize--;
        if(event.x >= 41 && event.x < 61 && selfield == 1 && fontsize < FONTMAXSIZE) fontsize++;
    }
    else wins[0].field = -1;
    selfield = -1;
}
